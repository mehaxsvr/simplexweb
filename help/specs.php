<?php

class FileContent
{
	private $rows = [];
	private $index = 0;
	
	public function __construct($filename)
	{
		$content = file_get_contents($filename);
		$this->rows = explode("\n", $content);
	}
	
	private function __nextLine()
	{
		if ($this->index < count($this->rows))
			return $this->rows[$this->index++];
		else
			return null;
	}
	
	public function nextLine($skipempty = true)
	{
		while(($row = $this->__nextLine()) !== null && (!$skipempty || str_replace('/[\s\t]+/g', '', $row) === ""));
		return $row;
	}
	
	public function back()
	{
		$this->index--;
	}
}
	
function match($row, $regex)
{
	$matches = [];
	preg_match($regex, $row, $matches);
	return $matches;
}

$regex1 = '/^([^:^\s^\t]+):\s*(.*)$/';
$regex2 = '/^[\s\t]+([^\s^\t]+)[\s\t]+(.*)$/';

$content = new FileContent("list of specs");
$specs = [];
while($row = $content->nextLine())
{
	$matches1 = match($row, $regex1);
	
	if (count($matches1) >= 2)
	{
		$new_spec = [
			'key' => $matches1[1],
			'name' => trim($matches1[2])
		];
		
		while($subrow = $content->nextLine())
		{
			$matches2 = match($subrow, $regex2);
			if (count($matches2) < 2)
				break;
			
			if (!isset($new_spec['options']))
				$new_spec['options'] = [];
			array_push($new_spec['options'], [
				'value' => $matches2[1],
				'description' => trim($matches2[2])
			]);
		}
		$content->back();
		
		array_push($specs, $new_spec);
	}
}

file_put_contents("output.json", json_encode($specs));
echo "done: " . count($specs) . PHP_EOL;
