import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    username = '';
    password = '';

    invalid = false;
    error_message = '';

    constructor(private authService: AuthService, private router: Router) {
      if (this.authService.getToken()) {
        this.router.navigate(['']);
      }
    }

    ngOnInit() {
    }

    login() {
      this.invalid = false;
        this.authService.authenticate(this.username, this.password, (data) => {
          this.router.navigate(['']);
        }, (error_message) => {
          this.invalid = true;
          this.error_message = error_message;
        });
    }
}
