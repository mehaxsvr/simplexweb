import { Component, OnInit } from '@angular/core';
import {DODetails, DigitaloceanService} from '../../services/digitalocean.service';
import {AuthService} from '../../services/auth.service';
import {User} from '../../models/user.model';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  loggedUser: User;
  details: DODetails;

  constructor(private doService: DigitaloceanService,
              private authService: AuthService,
              private activatedRoute: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.authService.getUser().subscribe(user => {
      this.loggedUser = user;

      this.activatedRoute.queryParams.subscribe(params => {
        const code = params['code'];
        if (code) {
          this.loggedUser.digitalocean_token = code;
          this.authService.setUser(this.loggedUser);
          this.router.navigate(['account']);
        }
      });
    });

    this.doService.getDetails().subscribe(response => {
      if (response.status === 'ok') {
        this.details = response.result;
      }
    });

    if (this.loggedUser.digitalocean_token) {
      this.doService.identify(this.loggedUser.digitalocean_token).subscribe(response => {
        if (response.status === 'ok') {
          console.log('identified');
        } else {
          console.log('not identified');
        }
      });
    }
  }

  logout() {
    this.loggedUser.digitalocean_token = null;
    this.authService.setUser(this.loggedUser);
  }

}
