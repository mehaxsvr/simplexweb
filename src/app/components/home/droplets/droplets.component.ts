import {Component, Inject, Injector, OnInit, PLATFORM_ID} from '@angular/core';
import {Response} from '../../../services/Response';
import {DropletsService} from '../../../services/droplets.service';
import {AuthService} from '../../../services/auth.service';
import {UsersService} from '../../../services/users.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {User} from '../../../models/user.model';
import {Droplet} from '../../../models/droplet.model';

@Component({
  selector: 'app-droplets',
  templateUrl: './droplets.component.html',
  styleUrls: ['./droplets.component.css']
})
export class DropletsComponent implements OnInit {
  loader = true;
  loggedUser: User;

  droplets: Droplet[];
  users: User[];

  selectedDroplet: Droplet = {
    id: null,
    name: '',
    user_id: null,
    do_id: null,
    do_created_at: null,
    host: '',
    server_ip: '',
    floating_ip: '',
    ssh_port: 22,
    ssh_user: '',
    ssh_pass: '',
    vcpus: null,
    disk: null,
    memory: null,
    www_dir: null,
  };
  selectedDropletKey: number;


  constructor(private dropletsService: DropletsService,
              private usersService: UsersService,
              private authService: AuthService,
              private modalService: NgbModal) {
  }

  ngOnInit() {
    this.authService.getUser().subscribe((user: User) => {
      this.loggedUser = user;
      if (user.level === 1) {
        this.usersService.get().subscribe((data: Response<User[]>) => {
          if (data.status === 'ok') {
            this.users = data.result;
          }
        }, error => this.authService.onError(error));
      }
    });
    this.dropletsService.get().subscribe((data: Response<Droplet[]>) => {
      this.droplets = data.result;
      this.loader = false;
    }, error => this.authService.onError(error));
  }

  openDroplet(content: any, droplet?: Droplet, droplet_key?: number): void {
    this.selectedDropletKey = droplet_key;

    this.selectedDroplet.id = droplet ? droplet.id : null;
    this.selectedDroplet.name = droplet ? droplet.name : '';
    this.selectedDroplet.user_id = droplet ? droplet.user_id : null;
    this.selectedDroplet.host = droplet ? droplet.host : '';
    this.selectedDroplet.server_ip = droplet ? droplet.server_ip : '';
    this.selectedDroplet.floating_ip = droplet ? droplet.floating_ip : '';
    this.selectedDroplet.ssh_port = droplet ? droplet.ssh_port : 22;
    this.selectedDroplet.ssh_user = droplet ? droplet.ssh_user : '';
    this.selectedDroplet.ssh_pass = droplet ? droplet.ssh_pass : '';
    this.selectedDroplet.vcpus = droplet ? droplet.vcpus : null;
    this.selectedDroplet.disk = droplet ? droplet.disk : null;
    this.selectedDroplet.memory = droplet ? droplet.memory : null;

    this.modalService.open(content);
  }

  saveDroplet(): void {
    if (this.selectedDroplet.id) {
      // update existing
      this.dropletsService.put(this.selectedDroplet.id, this.selectedDroplet).subscribe((response: Response<Droplet>) => {
        if (response.status === 'ok') {
          this.droplets[this.selectedDropletKey] = response.result;
          this.modalService.dismissAll();
        }
      }, error => this.authService.onError(error));
    } else {
      // insert new
      this.dropletsService.post(this.selectedDroplet).subscribe((response: Response<Droplet>) => {
        if (response.status === 'ok') {
          this.droplets.push(response.result);
          this.modalService.dismissAll();
        }
      }, error => this.authService.onError(error));
    }
  }

  removeDroplet(droplet: Droplet, droplet_key: number): void {
    if (confirm('Are you sure you want to remove \'' + droplet.name + '\' droplet?')) {
      this.dropletsService.delete(droplet.id).subscribe((response: Response<string>) => {
        if (response.status === 'ok') {
          this.droplets.splice(droplet_key, 1);
        }
      }, error => this.authService.onError(error));
    }
  }
}
