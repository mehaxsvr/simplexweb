import { Component, OnInit } from '@angular/core';
import {DomainsService} from '../../services/domains.service';
import {Response} from '../../services/Response';
import {DropletsService} from '../../services/droplets.service';
import {RecordsService} from '../../services/records.service';
import {HostsService} from '../../services/hosts.service';
import {AuthService} from '../../services/auth.service';
import {User} from '../../models/user.model';
import {Domain} from '../../models/domain.model';
import {Droplet} from '../../models/droplet.model';
import {Record} from '../../models/record.model';
import {Host} from '../../models/host.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  loggedUser: User;
  domains: Domain[];
  droplets: Droplet[];
  records: Record[];
  hosts: Host[];

  constructor(private authService: AuthService,
              private domainsService: DomainsService,
              private dropletsService: DropletsService,
              private recordsService: RecordsService,
              private hostsService: HostsService) {}

  ngOnInit() {
    this.authService.getUser().subscribe((data: User) => {
      this.loggedUser = data;
    });

    this.domainsService.get().subscribe((data: Response<Domain[]>) => {
      if (data.status === 'ok') {
        this.domains = data.result;
      }
    }, error => this.authService.onError(error));

    this.dropletsService.get().subscribe((data: Response<Droplet[]>) => {
      if (data.status === 'ok') {
        this.droplets = data.result;
      }
    }, error => this.authService.onError(error));

    this.recordsService.get().subscribe((data: Response<Record[]>) => {
      if (data.status === 'ok') {
        this.records = data.result;
      }
    }, error => this.authService.onError(error));

    this.hostsService.get().subscribe((data: Response<Host[]>) => {
      if (data.status === 'ok') {
        this.hosts = data.result;
      }
    }, error => this.authService.onError(error));
  }

}
