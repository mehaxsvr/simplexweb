import { Component, OnInit } from '@angular/core';
import {UsersService} from '../../../services/users.service';
import {Response} from '../../../services/Response';
import {AuthService} from '../../../services/auth.service';
import {DomainsService} from '../../../services/domains.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {RecordsService} from '../../../services/records.service';
import {User} from '../../../models/user.model';
import {Domain} from '../../../models/domain.model';
import {Record} from '../../../models/record.model';

@Component({
  selector: 'app-records',
  templateUrl: './records.component.html',
  styleUrls: ['./records.component.css']
})
export class RecordsComponent implements OnInit {
  loader = true;
  loggedUser: User;

  records: Record[];
  domains: Domain[];
  users: User[];

  selectedRecord: Record = {
    id: null,
    name: null,
    do_id: null,
    user_id: null,
    domain_id: null,
    type: '',
    data: null,
  };
  selectedRecordKey: number;

  constructor(private authService: AuthService,
              private recordsService: RecordsService,
              private usersService: UsersService,
              private domainsService: DomainsService,
              private modalService: NgbModal) { }

  ngOnInit() {
    this.authService.getUser().subscribe((user: User) => {
      this.loggedUser = user;
      if (user.level === 1) {
        this.usersService.get().subscribe((data: Response<User[]>) => {
          this.users = data.result;
        }, error => this.authService.onError(error));
      }
    });

    this.domainsService.get().subscribe((response: Response<Domain[]>) => {
      if (response.status === 'ok') {
        this.domains = response.result;
      }
    }, error => this.authService.onError(error));

    this.recordsService.get().subscribe((response: Response<Record[]>) => {
      if (response.status === 'ok') {
        this.records = response.result;
        this.loader = false;
      }
    }, error => this.authService.onError(error));
  }

  openRecord(content: any, record?: Record, record_key?: number): void {
    this.selectedRecordKey = record_key;

    this.selectedRecord.id = record ? record.id : null;
    this.selectedRecord.name = record ? record.name : null;
    this.selectedRecord.user_id = record ? record.user_id : null;
    this.selectedRecord.domain_id = record ? record.domain_id : null;
    this.selectedRecord.type = record ? record.type : null;
    this.selectedRecord.data = record ? record.data : '';

    this.modalService.open(content);
  }

  saveRecord(): void {
    if (this.selectedRecord.id) {
      this.recordsService.put(this.selectedRecord.id, this.selectedRecord).subscribe((response: Response<Record>) => {
        if (response.status === 'ok') {
          this.records[this.selectedRecordKey] = response.result;
          this.modalService.dismissAll();
        }
      });
    } else {
      this.recordsService.post(this.selectedRecord).subscribe((response: Response<Record>) => {
        if (response.status === 'ok') {
          this.records.push(response.result);
          this.modalService.dismissAll();
        }
      });
    }
  }

  removeRecord(record: Record, record_key: number): void {
    if (confirm('Are you sure you want to remove \'' + record.name + '\' record?')) {
      this.recordsService.delete(record.id).subscribe((response: Response<string>) => {
        this.records.splice(record_key, 1);
      });
    }
  }

}
