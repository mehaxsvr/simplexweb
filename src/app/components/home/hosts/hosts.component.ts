import { Component, OnInit } from '@angular/core';
import {UsersService} from '../../../services/users.service';
import {HostsService} from '../../../services/hosts.service';
import {AuthService} from '../../../services/auth.service';
import {DropletsService} from '../../../services/droplets.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Response} from '../../../services/Response';
import {RecordsService} from '../../../services/records.service';
import {VhostsService} from '../../../services/vhosts.service';
import {User} from '../../../models/user.model';
import {Host} from '../../../models/host.model';
import {Droplet} from '../../../models/droplet.model';
import {Record} from '../../../models/record.model';
import {Vhost} from '../../../models/vhost.model';

@Component({
  selector: 'app-hosts',
  templateUrl: './hosts.component.html',
  styleUrls: ['./hosts.component.css']
})
export class HostsComponent implements OnInit {
  loader = true;
  loggedUser: User;

  hosts: Host[];
  droplets: Droplet[];
  records: Record[];
  users: User[];

  selectedHost: Host = {
    id: null,
    name: null,
    droplet_id: null,
    user_id: null,
    record_id: null,
    site_name: null,
    www_dir: null,
    server_admin: null,
    access_log: null,
    error_log: null,
  };
  selectedHostKey: number;

  selectedVhost: Vhost = {
    id: null,
    host_id: null,

    server_name: null,
    server_alias: null,
    server_admin: null,
    document_root: null,
    access_log: null,
    error_log: null
  };
  selectedVhostKey: number;

  constructor(private authService: AuthService,
              private hostsService: HostsService,
              private dropletsService: DropletsService,
              private recordsService: RecordsService,
              private vhostsService: VhostsService,
              private usersService: UsersService,
              private modalService: NgbModal) { }

  ngOnInit() {
    this.authService.getUser().subscribe((user: User) => {
      this.loggedUser = user;
      if (user.level === 1) {
        this.usersService.get().subscribe((data: Response<User[]>) => {
          this.users = data.result;
        }, error => this.authService.onError(error));
      }
    });

    this.dropletsService.get().subscribe((response: Response<Droplet[]>) => {
      if (response.status === 'ok') {
        this.droplets = response.result;
      }
    });

    this.hostsService.get().subscribe((response: Response<Host[]>) => {
      if (response.status === 'ok') {
        this.hosts = response.result;
        this.loader = false;
      }
    }, error => this.authService.onError(error));

    this.recordsService.get().subscribe((response: Response<Record[]>) => {
      if (response.status === 'ok') {
        this.records = response.result;
      }
    });
  }

  openHost(content, host?, host_key?) {
    this.selectedHostKey = host_key;

    this.selectedHost.id = host ? host.id : null;
    this.selectedHost.name = host ? host.name : null;
    this.selectedHost.droplet_id = host ? host.droplet_id : null;
    this.selectedHost.user_id = host ? host.user_id : null;
    this.selectedHost.site_name = host ? host.site_name : null;
    this.selectedHost.www_dir = host ? host.www_dir : null;
    this.selectedHost.server_admin = host ? host.server_admin : null;
    this.selectedHost.access_log = host ? host.access_log : null;
    this.selectedHost.error_log = host ? host.error_log : null;

    this.modalService.open(content);
  }

  saveHost() {
    if (this.selectedHost.id) {
      this.hostsService.put(this.selectedHost.id, this.selectedHost).subscribe((response: Response<Host>) => {
        if (response.status === 'ok') {
          this.hosts[this.selectedHostKey] = response.result;
          this.modalService.dismissAll();
        }
      });
    } else {
      this.hostsService.post(this.selectedHost).subscribe((response: Response<Host>) => {
        if (response.status === 'ok') {
          this.hosts.push(response.result);
          this.modalService.dismissAll();
        }
      });
    }
  }

  removeHost(host, host_key) {
    if (confirm('Are you sure you want to remove \'' + host.name + '\' host?')) {
      this.hostsService.delete(host.id).subscribe((response: Response<string>) => {
        if (response.status === 'ok') {
          this.hosts.splice(host_key, 1);
        }
      });
    }
  }

  openVhost(content, host: Host, host_key: number, vhost?: Vhost, vhost_key?: number) {
    this.selectedVhostKey = vhost_key;
    this.selectedHost = host;
    this.selectedHostKey = host_key;

    this.selectedVhost.id = vhost ? vhost.id : null;
    this.selectedVhost.host_id = vhost ? vhost.host_id : host.id;
    this.selectedVhost.server_name = vhost ? vhost.server_name :
      (host.record.name !== '@' ? host.record.name + '.' : '') + host.record.domain.name;
    this.selectedVhost.server_alias = vhost ? vhost.server_alias :
      (host.record.name !== '@' ? host.record.name : 'www') + '.' + host.record.domain.name;
    this.selectedVhost.server_admin = vhost ? vhost.server_admin : host.server_admin;
    this.selectedVhost.document_root = vhost ? vhost.document_root : host.droplet.www_dir + host.www_dir + '/';
    this.selectedVhost.access_log = vhost ? vhost.access_log : host.access_log;
    this.selectedVhost.error_log = vhost ? vhost.error_log : host.error_log;

    this.modalService.open(content);
  }

  saveVhost() {
    if (this.selectedVhost.id) {
      this.vhostsService.put(this.selectedVhost.id, this.selectedVhost).subscribe((response: Response<Vhost>) => {
        if (response.status === 'ok') {
          this.selectedHost.vhosts[this.selectedVhostKey] = response.result;
          this.modalService.dismissAll();
        }
      });
    } else {
      this.vhostsService.post(this.selectedVhost).subscribe((response: Response<Vhost>) => {
        if (response.status === 'ok') {
          this.selectedHost.vhosts.push(response.result);
          this.modalService.dismissAll();
        }
      });
    }
  }

  removeVhost(host: Host, vhost: Vhost, vhost_key: number) {
    if (confirm('Are you sure you want to remove \'' + vhost.server_name + '\' vhost?')) {
      this.vhostsService.delete(vhost.id).subscribe((response: Response<string>) => {
        if (response.status === 'ok') {
          host.vhosts.splice(vhost_key, 1);
        }
      });
    }
  }

}
