import { Component, OnInit } from '@angular/core';
import {Response} from '../../../services/Response';
import {DomainsService} from '../../../services/domains.service';
import {UsersService} from '../../../services/users.service';
import {AuthService} from '../../../services/auth.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Domain} from '../../../models/domain.model';
import {User} from '../../../models/user.model';
import {Record} from '../../../models/record.model';
import {RecordsService} from '../../../services/records.service';

@Component({
  selector: 'app-domains',
  templateUrl: './domains.component.html',
  styleUrls: ['./domains.component.css']
})
export class DomainsComponent implements OnInit {
  loader = true;
  loggedUser: User;

  domains: Domain[];
  users: User[];

  selectedDomain: Domain = {
    id: null,
    name: '',
    ttl: null,
    user_id: null
  };
  selectedDomainKey: number;

  selectedRecord: Record = {
    id: null,
    name: null,
    do_id: null,
    user_id: null,
    domain_id: null,
    type: '',
    data: null,
  };
  selectedRecordKey: number;

  constructor(private domainsService: DomainsService,
              private recordsService: RecordsService,
              private usersService: UsersService,
              private authService: AuthService,
              private modalService: NgbModal) { }

  ngOnInit() {
    this.authService.getUser().subscribe((user: User) => {
      this.loggedUser = user;
      if (user.level === 1) {
        this.usersService.get().subscribe((data: Response<User[]>) => {
          this.users = data.result;
        }, error => this.authService.onError(error));
      }
    });
    this.domainsService.get({with: 'records'}).subscribe((response: Response<Domain[]>) => {
      if (response.status === 'ok') {
        this.domains = response.result;
        this.loader = false;
      }
    }, error => this.authService.onError(error));
  }

  openDomain(content: any, domain?: Domain, domain_key?: number): void {
    this.selectedDomainKey = domain_key;

    this.selectedDomain.id = domain ? domain.id : null;
    this.selectedDomain.name = domain ? domain.name : '';
    this.selectedDomain.user_id = domain ? domain.user_id : null;
    this.selectedDomain.ttl = domain ? domain.ttl : null;

    this.modalService.open(content);
  }

  saveDomain(): void {
    if (this.selectedDomain.id) {
      this.domainsService.put(this.selectedDomain.id, this.selectedDomain).subscribe((response: Response<Domain>) => {
        if (response.status === 'ok') {
          this.domains[this.selectedDomainKey] = response.result;
          this.modalService.dismissAll();
        }
      });
    } else {
      this.domainsService.post(this.selectedDomain).subscribe((response: Response<Domain>) => {
        if (response.status === 'ok') {
          this.domains.push(this.selectedDomain);
          this.modalService.dismissAll();
        }
      });
    }
  }

  removeDomain(domain: Domain, domain_key: number): void {
    if (confirm('Are you sure you want to remove \'' + domain.name + '\' domain?')) {
      this.domainsService.delete(domain.id).subscribe((response: Response<string>) => {
        if (response.status === 'ok') {
          this.domains.splice(domain_key, 1);
        }
      });
    }
  }


  openRecord(content: any, domain: Domain, domain_key: number, record?: Record, record_key?: number): void {
    this.selectedRecordKey = record_key;
    this.selectedDomain = domain;
    this.selectedDomainKey = domain_key;

    this.selectedRecord.id = record ? record.id : null;
    this.selectedRecord.name = record ? record.name : null;
    this.selectedRecord.user_id = record ? record.user_id : null;
    this.selectedRecord.domain_id = record ? record.domain_id : domain.id;
    this.selectedRecord.type = record ? record.type : null;
    this.selectedRecord.data = record ? record.data : '';

    this.modalService.open(content);
  }

  saveRecord(): void {
    if (this.selectedRecord.id) {
      this.recordsService.put(this.selectedRecord.id, this.selectedRecord).subscribe((response: Response<Record>) => {
        if (response.status === 'ok') {
          this.selectedDomain.records[this.selectedRecordKey] = response.result;
          this.modalService.dismissAll();
        }
      });
    } else {
      this.recordsService.post(this.selectedRecord).subscribe((response: Response<Record>) => {
        if (response.status === 'ok') {
          this.selectedDomain.records.push(response.result);
          this.modalService.dismissAll();
        }
      });
    }
  }

  removeRecord(record: Record, record_key: number): void {
    if (confirm('Are you sure you want to remove \'' + record.name + '\' record?')) {
      this.recordsService.delete(record.id).subscribe((response: Response<string>) => {
        this.selectedDomain.records.splice(record_key, 1);
      });
    }
  }

  recordTypeChanged() {
    switch (this.selectedRecord.type) {
      case 'CNAME':
        this.selectedRecord.data = '@';
        break;
    }
  }

}
