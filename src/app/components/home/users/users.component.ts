import { Component, OnInit } from '@angular/core';
import {UsersService} from '../../../services/users.service';
import {AuthService} from '../../../services/auth.service';
import {Router} from '@angular/router';
import {Response} from '../../../services/Response';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {User} from '../../../models/user.model';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  loader = true;

  users: User[];
  selectedUser: User = {
    id: null,
    name: '',
    email: '',
  };
  selectedUserKey: number;

  loggedUser: User;

  constructor(private authService: AuthService,
              private router: Router,
              private usersService: UsersService,
              private modalService: NgbModal) { }

  ngOnInit() {
    this.authService.getUser().subscribe((user: User) => {
      this.loggedUser = user;
      if (user.level !== 1) {
        this.router.navigate(['']);
      }

      this.usersService.get().subscribe((response: Response<User[]>) => {
        if (response.status === 'ok') {
          this.users = response.result;
          this.loader = false;
        }
      }, error => this.authService.onError(error));
    }, error => this.authService.onError(error));
  }

  openUser(content: any, user?: User, user_key?: number): void {
    this.selectedUserKey = user_key;

    this.selectedUser.id = user ? user.id : null;
    this.selectedUser.name = user ? user.name : '';
    this.selectedUser.email = user ? user.email : '';

    this.modalService.open(content);
  }

  saveUser(): void {
    if (this.selectedUser.id) {
      this.usersService.put(this.selectedUser.id, this.selectedUser).subscribe((response: Response<User>) => {
        if (response.status === 'ok') {
          this.users[this.selectedUserKey] = response.result;
          this.modalService.dismissAll();
        }
      }, error => this.authService.onError(error));
    } else {
      this.usersService.post(this.selectedUser).subscribe((response: Response<User>) => {
        if (response.status === 'ok') {
          this.users.push(response.result);
          this.modalService.dismissAll();
        }
      }, error => this.authService.onError(error));
    }
  }

  removeUser(user: User, user_key: number): void {
    if (confirm('Are you sure you want to remove \'' + user.name + '\' user?')) {
      this.usersService.delete(user.id).subscribe((response: Response<string>) => {
        this.users.splice(user_key, 1);
      });
    }
  }

}
