export interface Response<T> {
    result?: T;
    status: string;
    message?: string;
}
