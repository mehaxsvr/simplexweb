import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Response} from './Response';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {User} from '../models/user.model';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    constructor(private http: HttpClient, private router: Router) {
      if (!this.getToken()) {
        this.router.navigate(['/login']);
      }
    }

    getToken(): string {
        return localStorage.getItem('token');
    }

    setToken(token: string = ''): void {
        localStorage.setItem('token', token);
    }

    getUser(): Observable<User> {
      return new Observable<User>((observer) => {
        observer.next(JSON.parse(localStorage.getItem('user')));
        observer.complete();
      });
    }

    setUser(user: User): void {
      localStorage.setItem('user', JSON.stringify(user));
    }

    authenticate(username: string, password: string, callback: Function, callback_error: Function) {
        this.http.post(environment.apiUrl + 'login', {email: username, password: password}).subscribe((data: Response<LoginData>) => {
            if (data.status === 'ok') {
                this.setToken(data.result.token);
                this.http.get(environment.apiUrl + 'details').subscribe((details: Response<User>) => {
                    this.setUser(details.result);
                    callback(data.result.token, details.result);
                });
            } else {
                callback_error(data.message ? data.message : 'Something went wrong');
            }
        }, (response) => {
            callback_error(response.error.message ? response.error.message : 'Service down');
        });
    }

    onError(error): void {
      if (error.status === 401) {
        this.logout();
      }
    }

    logout(): void {
      this.setToken();
      this.router.navigate(['/login']);
    }
}

interface LoginData {
    token: string;
}
