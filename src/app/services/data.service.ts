import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {AuthService} from './auth.service';

export abstract class DataService {
  private authService: AuthService;

  protected serviceName: string;
  protected authentication = true;

  constructor(private http: HttpClient, serviceName) {
    this.serviceName = serviceName;
  }

  get(data = {}) {
    return this.http.get(environment.apiUrl + this.serviceName, {params: data});
  }

  getOne(id: number, data = {}) {
    return this.http.get(environment.apiUrl + this.serviceName + '/' + id, {params: data});
  }

  post(data) {
    return this.http.post(environment.apiUrl + this.serviceName, data);
  }

  put(id: number, data) {
    return this.http.put(environment.apiUrl + this.serviceName + '/' + id, data);
  }

  delete(id: number, data = {}) {
    return this.http.delete(environment.apiUrl + this.serviceName + '/' + id, {params: data});
  }
}
