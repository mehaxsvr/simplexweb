import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Response} from './Response';

@Injectable({
  providedIn: 'root'
})
export class DigitaloceanService {

  constructor(private http: HttpClient) { }

  getDetails() {
    return this.http.get<Response<DODetails>>(environment.apiUrl + 'digitalocean/details');
  }

  identify(code) {
    return this.http.get<Response<boolean>>(environment.apiUrl + 'digitalocean/identify/' + code);
  }
}

export interface DODetails {
  client_id: string;
  redirect_url: string;
  response_type: string;
  url: string;
}
