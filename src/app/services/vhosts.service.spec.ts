import { TestBed, inject } from '@angular/core/testing';

import { VhostsService } from './vhosts.service';

describe('VhostsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VhostsService]
    });
  });

  it('should be created', inject([VhostsService], (service: VhostsService) => {
    expect(service).toBeTruthy();
  }));
});
