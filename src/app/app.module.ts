import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {LoginComponent} from './components/login/login.component';
import {RouterModule, Routes} from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import {AuthGuard} from './guards/auth.guard';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {TokenInterceptor} from './interceptors/token.interceptor';
import { LogoutComponent } from './components/logout/logout.component';
import { DropletsComponent } from './components/home/droplets/droplets.component';
import { DomainsComponent } from './components/home/domains/domains.component';
import { HostsComponent } from './components/home/hosts/hosts.component';
import { RecordsComponent } from './components/home/records/records.component';
import { UsersComponent } from './components/home/users/users.component';
import { IndexComponent } from './components/home/index/index.component';
import { DefaultPipe } from './pipes/default.pipe';
import { AddeditPipe } from './pipes/addedit.pipe';
import { NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AccountComponent } from './components/account/account.component';

const appRoutes: Routes = [
    {
        path: 'home',
        component: HomeComponent,
        canActivate: [AuthGuard],
        children: [
          {path: '', redirectTo: 'droplets', pathMatch: 'full'},
          {path: 'droplets', component: DropletsComponent, canActivate: [AuthGuard]},
          {path: 'domains', component: DomainsComponent, canActivate: [AuthGuard]},
          {path: 'hosts', component: HostsComponent, canActivate: [AuthGuard]},
          {path: 'records', component: RecordsComponent, canActivate: [AuthGuard]},
          {path: 'users', component: UsersComponent, canActivate: [AuthGuard]},
        ]
    },
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full',
    },
    {
        path: '*',
        redirectTo: ''
    },
    {
      path: 'login',
      component: LoginComponent
    },
    {
        path: 'logout',
        component: LogoutComponent
    },
    {
        path: 'account',
        component: AccountComponent,
        canActivate: [AuthGuard]
    },
];

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        HomeComponent,
        LogoutComponent,
        DropletsComponent,
        DomainsComponent,
        HostsComponent,
        RecordsComponent,
        UsersComponent,
        IndexComponent,
        DefaultPipe,
        AddeditPipe,
        AccountComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        FormsModule,
        NgbModule,
        RouterModule.forRoot(appRoutes)
    ],
    providers: [
      {
        provide: HTTP_INTERCEPTORS,
        useClass: TokenInterceptor,
        multi: true
      }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
