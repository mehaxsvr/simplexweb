import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'default'
})
export class DefaultPipe implements PipeTransform {

  transform(value: string, _default: string = 'N/A'): string {
    return value ? value : _default;
  }

}
