import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'addedit'
})
export class AddeditPipe<T> implements PipeTransform {

  transform(value: T, fields: string[], _default = null): T {
    // @ts-ignore
    const res: T = {};
    fields.forEach((field) => {
      res[field] = value ? value[field] : _default;
    });
    return res;
  }
}
