import {User} from './user.model';
import {Record} from './record.model';

export class Domain {
  id: number;
  name: string;
  ttl?: number;
  user_id?: number;

  user?: User;
  records?: Record[];
}
