import {Host} from './host.model';
import {User} from './user.model';

export class Vhost {
  id: number;
  host_id: number;
  user_id?: number;

  server_name: string;
  server_alias: string;
  server_admin: string;
  document_root: string;
  access_log: string;
  error_log: string;

  host?: Host;
  user?: User;
}
