import {User} from './user.model';


export class Droplet {
  id: number;
  user_id?: number;
  name: string;
  host: string;
  server_ip: string;
  floating_ip?: string;
  do_id?: number;
  do_created_at: string;
  ssh_port?: number;
  ssh_user?: string;
  ssh_pass?: string;
  vcpus?: number;
  memory?: number;
  disk?: number;
  www_dir: string;

  user?: User;
}
