export class User {
  id: number;
  name: string;
  level?: number;
  email: string;

  digitalocean_token?: string;
}
