import {Droplet} from './droplet.model';
import {Vhost} from './vhost.model';
import {User} from './user.model';
import {Record} from './record.model';

export class Host {
  id: number;
  name: string;
  www_dir?: string;
  site_name?: string;
  server_admin?: string;
  access_log?: string;
  error_log?: string;
  droplet_id: number;
  user_id?: number;
  record_id: number;

  droplet?: Droplet;
  user?: User;
  record?: Record;
  vhosts?: Vhost[];
}
