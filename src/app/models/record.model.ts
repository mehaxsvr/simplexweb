import {Domain} from './domain.model';
import {User} from './user.model';

export class Record {
  id: number;
  do_id: number;
  name: string;
  user_id?: number;
  domain_id: number;
  type: string;
  data?: string;

  user?: User;
  domain?: Domain;
}
